const board = document.querySelector('#boaed');
// Adjusting the cells in the container
const SQUARES_NUMBER = 320; 
// Array with colors
const colors = ['#3E94D1', '#65A5D1', '#4E51D8', '#7375D8'];

// The main function for working with the number of cells and with random colors on hover
for (let i = 0; i < SQUARES_NUMBER; i++) {
    const square = document.createElement('div');
    square.classList.add('square');
    
square.addEventListener('mouseover', () =>  
setColor(square));
square.addEventListener('mouseleave', () =>  
removeColor(square));

board.append(square);
}
// Function for changing the shadow color
function setColor(element) {
    const color = getColorRendom();
    element.style.backgroundColor = color;
    element.style.boxShadow = `0 0 2px ${color}, 0 0 10px ${color}`;
}
// Function to return to the previous shade color
function removeColor(element) {
    element.style.backgroundColor = '#1d1d1d';
    element.style.boxShadow = `0 0 2px #000`;
}
// Function for random color selection
function getColorRendom() {
   const index = Math.floor(Math.random() * colors.length);
    return colors[index];
}
